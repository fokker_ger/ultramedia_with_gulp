var gulp         = require('gulp'),//Подключаем gulp пакет
    less         = require('gulp-less'),//Подключаем less пакет
    sourcemaps   = require('gulp-sourcemaps'),
    browserSync  = require('browser-sync'), // Подключаем Browser Sync
    concat       = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации
    uglify       = require('gulp-uglifyjs'), // Подключаем gulp-uglifyjs (для сжатия JS)
    cssnano      = require('gulp-cssnano'), // Подключаем пакет для минификации CSS
    rename       = require('gulp-rename'), // Подключаем библиотеку для переименования
    del          = require('del'), // Подключаем библиотеку для удаления файлов и папок
    imagemin     = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
    pngquant     = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
    cache        = require('gulp-cache'), // Подключаем библиотеку кеширования
    autoprefixer = require('gulp-autoprefixer');// Подключаем библиотеку для автоматич

gulp.task('less', function() { //Создаем task "less"
    return gulp.src(['app/less/**/*.less', '!app/less/const']) //Берем источник. Берем все less файлы и з папки less и дочерних папок
        .pipe(sourcemaps.init())
        .pipe(less())//Преобразуем less в css посредством gulp-less
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) // Создаем префиксы
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('app/css'))// Выгружаем результат в папку app/css
        .pipe(browserSync.reload({stream: true})) //Обновляем CSS на странице при изменении less
});
gulp.task('browser-sync', function() { // Создаем таск browser-sync
    browserSync({ // Выполняем browser Sync
        server: { // Определяем параметры сервера
            baseDir: 'app' // Директория для сервера - app
        },
        notify: false // Отключаем уведомления
    });
});
gulp.task('scripts', function() {
    return gulp.src([ // Берем все необходимые библиотеки
        'app/libs/jquery/dist/jquery.min.js', // Берем jQuery
        'app/libs/swiper/dist/js/swiper.min.js', // Берем Swiper
        'app/libs/fancybox/dist/jquery.fancybox.min.js' // Берем fancybox
    ])
        .pipe(concat('libs.min.js')) // Собираем их в кучу в новом файле libs.min.js
        .pipe(uglify()) // Сжимаем JS файл
        .pipe(gulp.dest('app/js')); // Выгружаем в папку app/js
});
gulp.task('code', function() {
    return gulp.src('app/*.html')
        .pipe(browserSync.reload({ stream: true }))
});
gulp.task('css-libs', function() {
    return gulp.src('app/less/libs/libs.less') // Выбираем файл для минификации
        .pipe(less()) // Преобразуем less в CSS посредством gulp-sass
        .pipe(cssnano()) // Сжимаем
        .pipe(rename({suffix: '.min'})) // Добавляем суффикс .min
        .pipe(gulp.dest('app/css/libs')); // Выгружаем в папку app/css
});

gulp.task('clean', async function() {
    return del.sync('dist'); // Удаляем папку dist перед сборкой
});
gulp.task('img', function() {
    return gulp.src('app/images/**/*') // Берем все изображения из app
        .pipe(cache(imagemin({ // С кешированием
            // .pipe(imagemin({ // Сжимаем изображения без кеширования
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))/**/)
        .pipe(gulp.dest('dist/images')); // Выгружаем на продакшен
});
gulp.task('build', async function() {

    var buildCss = gulp.src([ // Переносим библиотеки в продакшен
        'app/css/**/*.css',
        'app/css/libs/libs.min.css'
    ])
        .pipe(gulp.dest('dist/css'))

    var buildFonts = gulp.src('app/fonts/**/*') // Переносим шрифты в продакшен
        .pipe(gulp.dest('dist/fonts'))

    var buildJs = gulp.src('app/js/**/*') // Переносим скрипты в продакшен
        .pipe(gulp.dest('dist/js'))

    var buildHtml = gulp.src('app/*.html') // Переносим HTML в продакшен
        .pipe(gulp.dest('dist'))

    var buildPhp = gulp.src('app/*.php') // Переносим PHP в продакшен
        .pipe(gulp.dest('dist'));

});
gulp.task('clear', function (callback) {
    return cache.clearAll('dist');
})
gulp.task('watch', function() { //Сначала запускается less затем  browser-sync, а потом уже watch
    gulp.watch('app/less/**/*.less', gulp.parallel('less'));// Наблюдение за less файлами
    // Наблюдение за другими типами файлов
    gulp.watch('app/*.html', gulp.parallel('code')); // Наблюдение за HTML файлами
    gulp.watch(['app/js/common.js', 'app/libs/**/*.js'], gulp.parallel('scripts')); // Наблюдение за главным JS файлом и за библиотеками
});
gulp.task('default', gulp.parallel('css-libs','less', 'scripts', 'browser-sync', 'watch')); //Сначала запускается less затем  browser-sync, а потом уже watch
gulp.task('build', gulp.parallel('build', 'clean', 'img', 'less', 'scripts'));