$(function () {
    /*Fixed Header*/
    const $navLinks = $("[data-scroll]");
    let header = $("#header");
    let scrollPossition = $(window).scrollTop();/*Какой у нас текущий скрол, т.е при обновлении или открытии страницы*/
    let intro = $("#intro");
    let introH = intro.innerHeight()  /* = $("#intro").innerHeight();/*Переменная для измерения высоты блока*/
    let nav = $("#nav");
    let navToggle = $("#navToggle");
    let navLink = $(".nav__link");
    checkScroll(scrollPossition, introH);/*Вызываем функцию при загрузке страницы*/
    $(window).on("scroll resize", function () {
        introH = intro.innerHeight();/*Обновление переменной при обновлении страницы и изменении размера окна*/
        scrollPossition = $(this).scrollTop();
        checkScroll(scrollPossition, introH); /*Передаем в функцию параметры. И при скроле будем вызывать эту функцию */
    });
    function checkScroll(scrollPossition, introH) { /*Принимаем параметры Функция проверки чтобы не пропадала шапка. */
        if (scrollPossition >= introH) {
            header.addClass("header--fixed"); /*Добавление  класса с фиксированным меню*/
        }
        else {
            header.removeClass("header--fixed");
        }
    }

    /*Smooth scroll*/
    $navLinks.on("click", function (event) {
        const $self = $(event.currentTarget);
        event.preventDefault(); /*Отменяем обычное поведение ссылк, т.е. при нажатии на пункт меню, страница не будет перезагружаться*/
        $navLinks.removeClass('active');
        $self.addClass('active');
        let elementId = $(this).data('scroll');
        /*console.log(elementId); Тут можно подробно посмотреть что присваивается в переменную при клике на пункт меню*/
        let elementOffset = $(elementId).offset().top; /*Получаем позицию для section от верха страницы, для скрола. На какую высоту скролить при нажатии на пункт меню*/
        nav.removeClass("show");
        /*console.log(elementOffset) Можно посмотреть какие позиции показывает от верха страницы каждый нажатый пункт меню*/
        $("html, body").animate({
            scrollTop: elementOffset /*Скрол на значение переменной от верха*/
        }, 700); /*Скорость скрола*/
        console.log(linkActive);
    });

    navToggle.on("click", function (event) {
        event.preventDefault();
        nav.toggleClass("show");
    })
});